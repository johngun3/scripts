
## Find current script path
script_path="UNKNOWN_PLATFORM"
unamestr=`uname`
if [ "$unamestr" == 'Linux' ]; then
    script_path=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
elif [ "$unamestr" == 'Darwin' ]; then
    script_path=$(dirname $(greadlink -f ${BASH_SOURCE[0]}))
fi

## Git Aliases

# git fetch
alias gf='git fetch'
alias gfp='git fetch -p'
alias gft='git fetch --tags'

# git pull
alias gpull='git pull origin'

# git checkout
alias gco='$script_path/fuzzy_checkout.py'
alias gcob='git checkout -b'

# git merge
alias gm='git merge'

# git add
alias ga='git add'
alias gau='git add -u'

# git commit
alias gcm='git commit -m'
alias gca='git commit --amend --no-edit'
alias gcae='git commit --amend'

# git push
alias gpush='git push origin'
alias gph='git push -u origin HEAD'
alias yolo='git push -f origin'
alias yoloh='git push -f origin HEAD'
alias gpt='git push --tags'

# git status
alias gs='git status'

# git grep
alias gg='git grep'
alias ggr='git grep --recurse-submodules'

# git reset
alias grh='git reset --hard'
alias grhh='git reset --hard HEAD'

# git clean
alias gc='git clean -fdx'
alias gcf='git clean -ffdx'

# git rebase
alias gr='git rebase'
alias gri='git rebase -i'
alias grc='git rebase --continue'
alias gra='git rebase --abort'

# git stash
alias gst='git stash'
alias gstp='git stash pop'

# git branch
alias gb='git branch'

# Show branches in repo sorted by date of last commit. Allows for arguments to git branch to be appended: "gbd -r"
alias gbd='! f() { for k in $(git branch $@ | sed "s/^..//; s/ .*//"); do echo "$(git log -1 --pretty='"'"'%Cgreen%ci %Cblue(%cr)%Creset '"'"' $k) $k" ; done | sort -r; }; f'

# git log
alias gl='git log'
alias glol='git log --graph --decorate --pretty=oneline --abbrev-commit --abbrev=10 --all'

# git diff
alias gd='git diff'
alias gdh='git diff HEAD'

# git show
alias gsh='git show HEAD'

#git submodule
alias gsubmod='git submodule init && git submodule update'

#git cherry-pick
alias gcp='git cherry-pick'
alias gcpa='git cherry-pick --abort'
alias gcpc='git cherry-pick --continue'

# git prune
alias gprune='git prune'

# git garbage collect
alias ggc='git gc'

# git reflog
alias grefl='git reflog'
