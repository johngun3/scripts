#!/usr/bin/env python
"""
git fuzzy-checkout
Same as `git checkout branch`, but with fuzzy matching if checkout fails.
Turns `git checkout barnch` into `git checkout branch`,
assuming `branch` is a branch.
"""

import difflib
import re
import sys

from subprocess import check_output, check_call, CalledProcessError

def git_branches():
    """construct list of git branches"""

    # Get local branches
    local_branch_text = check_output(["git", "branch"], universal_newlines=True).strip()
    local_branches = [line.lstrip('*').strip() for line in local_branch_text.splitlines()]

    # get remote branches. Strip off remote name
    remote_branch_text = check_output(["git", "branch", "-r"], universal_newlines=True).strip()
    remote_branches = [re.sub('^\w+/', '', line.strip()) for line in remote_branch_text.splitlines()]

    return list(set(local_branches).union(remote_branches)) 

def proximity(a, b):
    """measure proximity of two strings"""
    #if b in a or a in b:
    #  return 0.9
    return difflib.SequenceMatcher(None, a, b).ratio()

def fuzzy_checkout(input_args):
    """
    Wrapper for git-checkout that does fuzzy-matching
    Helps with typos, etc. by automatically checking out the closest match
    if the initial checkout call fails.
    """
    try:
        check_call(["git", "checkout"] + input_args[1:])
    except CalledProcessError:
        branch = input_args[1]
        branches = git_branches()
        branches = sorted(branches, key=lambda b : proximity(branch, b))
        best = branches[-1]
        if proximity(best, branch) > 0.6:
            print("Best match for '%s': '%s' (%.1f%%)" % (branch, best, 100*proximity(best, branch)))
            try:
                check_call(["git", "checkout", best] + input_args[2:])
            except CalledProcessError:
                return 1
        else:
            print("No good match found for '{}'.".format(branch))
    return 0

if __name__ == '__main__':
    sys.exit(fuzzy_checkout(sys.argv))
