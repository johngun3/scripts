# Scripts

This is a random amalgamation of scripts that I wrote or borrowed and modified to increase my productivity

## Branch Cleaner

Helps you clean up the branches in your local git repository. Just provide a path to a git repository on your local file system and it will prompt you to delete branches both locally and remotely.

## Bash Aliases

A file full of bash alias for shortening git commands. All you need to do is symlink this file to ~/.bash_aliases in your home directory and enjoy. ```ln -s bash_aliases ~/.bash_aliases```

This script employs the use of ```fuzzy_checkout.py``` to help you checkout branches with fewer mistakes.

### Mac Users

Mac users need to install greadlink (via coreutils) using homebrew.

    brew install coreutils

Mac users also need to create or append to a ```~/.bash_profile``` file with the following line:

    if [ -f $(greadlink ~/.bash_aliases) ]; then . ~/.bash_aliases; fi

## Fuzzy Checkout

Allows the caller to pass in a branch name followed by other command line arguments that will be passed to ```git checkout```. It attempts to checkout the specified branch or find the best match.
