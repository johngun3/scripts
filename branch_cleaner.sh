#!/bin/bash

echo "##########################################"
echo "##"
echo "##  Git Branch Cleanup"
echo "##"
echo "##  Cleans both local and remote branches."
echo "##  Press y to delete local branch or"
echo "##  press Y to delete both the local and"
echo "##  remote branches."
echo "##"
echo "##########################################\n"
 
repo_dir=$1
cd $repo_dir

echo "Delete branch (Y/y/n) [ENTER]:"

for branch in $(git for-each-ref --format='%(refname:short)' refs/heads/); do
  echo -ne "$branch "
  read decision

  if [ $decision == "y" ]; then
  	git branch -D $branch > /dev/null
  elif [ $decision == "Y" ]; then
	git branch -D $branch > /dev/null
	git push origin :$branch > /dev/null
  fi	
done
